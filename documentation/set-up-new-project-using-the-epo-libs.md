#### Set up a new Frontend module in dev



#### Create a new Maven Module

In IntelliJ, context menu on the main project, choose "New..."  "Module..."
Create "allesboek-web-gui"

Copy the pom.xml from the demo project and ajust all paths
Same for nginx-config/default.conf
Ajust the location entry
    location /allesboek/config/ {
        root /usr/share/nginx/app-config;
    }

Add the module to the <modules> section in the main pom.xml



#### Create a new Angular project
cd allesboek-web-gui

Make sure your global npm packages are up to date
npm -g update

#### Create the project and run it
ng new allesboek
cd allesboek
ng serve --port 4201


#### Create symlinks to the lib directories (Look into: how to do this with npm link)
cd allesboek-web-gui/allesboek/node_modules/


#### First, create links in the global node_modules - /usr/local/lib/node_modules/
If this is already done for another project, you can skip this step
npm link /Users/epo/Development/Projects/angular-libraries/dist/auth-lib
npm link /Users/epo/Development/Projects/angular-libraries/dist/entity-lib


# Link to the link in the global node_modules
npm link auth-lib
npm link entity-lib


# Add auth-lib and entity-lib to package.json
  "dependencies": {
  	...
    "auth-lib": "^0.0.1",
    "entity-lib": "^0.0.1",
    ...
  }

# Add preserveSymlinks: true to angular.json



# Install peerDependencies for the libs


# Add Angular Material
ng add @angular/material

# ? Choose a prebuilt theme name, or "custom" for a custom theme: Indigo/Pink        [ Preview: https://material.angular.io?theme=indigo-pink ]
# ? Set up global Angular Material typography styles? Yes
# ? Set up browser animations for Angular Material? Yes


# Install Moment
npm install moment --save


# Install the material-moment-adapter to be able to use moment in Angular Material
npm install @angular/material-moment-adapter --save


# Add oidc-client for the auth-lib
npm install oidc-client --save


# Add the NGX Logging library
npm install ngx-logger --save


# Copy over the configuration from the demo project

src/app/app-config.service.ts

src/environments/common-environment.ts
src/environments/environment.prod.ts
src/environments/environment.ts


src/assets/auth-silent-refresh-callback.html
src/assets/app-config.json



# Configuration
Update docker-compose

# Traefik
The labels in the docker-compose should do the job

# Keycloak
Create a client for allesboek-api and allesboek-gui













#### Later: write a how to like this for production

# OS
Create entries in /etc/hosts for allesboek.localhost.lab







