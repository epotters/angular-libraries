// Source: https://github.com/OasisDigital/angular-material-search-select/blob/master/src/app/search-select/base.ts
import {FocusMonitor} from '@angular/cdk/a11y';
import {ChangeDetectionStrategy, Component, ElementRef, forwardRef, Injector, Input, OnDestroy, ViewEncapsulation} from '@angular/core';
import {FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';
import {MatFormFieldControl} from '@angular/material/form-field';
import {Router} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {take, takeUntil} from 'rxjs/operators';
import {EntityMeta, EntityService, Identifiable} from '..';
import {EntityDataSource} from '../entity-data-source';
import {AbstractMatFormFieldControl} from './abstract-mat-form-field-control';


@Component({
  selector: 'entity-selector',
  templateUrl: './entity-selector.component.html',
  providers: [{
    provide: MatFormFieldControl,
    useExisting: EntitySelectorComponent
  }, {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => EntitySelectorComponent),
    multi: true
  }],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntitySelectorComponent<T extends Identifiable> extends AbstractMatFormFieldControl<T> implements OnDestroy {

  static controlType: string = 'entity-selector';

  @Input() meta: EntityMeta<T>;
  @Input() service: EntityService<T>;
  @Input() autoActiveFirstOption: boolean = false;
  @Input() showClearButton: boolean = true;
  @Input() emptyText: string = 'Create new entity';

  dataSource: EntityDataSource<T>;
  searchControl: FormControl = new FormControl();

  createButtonVisible: boolean = false;
  lengthToShowCreate: number = 2;

  skipFirstChange: boolean = false;
  firstChange: boolean = true;


  constructor(
    elementRef: ElementRef<HTMLElement>,
    focusMonitor: FocusMonitor,
    public injector: Injector,
    public router: Router,
    public logger: NGXLogger
  ) {
    super(elementRef, focusMonitor, injector, EntitySelectorComponent.controlType, logger);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }


  focus() {
    this.logger.debug('focus. current value:', this.value);
    this.focused = true;
  }

  blur() {
    this.logger.debug('blur. current value:', this.value);
    this.onTouched();
    this.focused = false;
  }

  optionSelected(event) {
    this.logger.debug('The user selected an option:', event.source.value);
    this.value = event.source.value;
  }

  public displayWith(controlValue?: T | string | null): string {
    if (controlValue == null) {
      return '';
    } else if (typeof controlValue === 'string') {
      return controlValue;
    } else if (!!controlValue.id) {
      return this.meta.displayNameRenderer(controlValue);
    } else {
      this.logger.warn('Unrecognized control value', controlValue);
      return '';
    }
  }

  private activateControl() {

    this.dataSource = new EntityDataSource<T>(this.meta, this.service, this.logger);

    this.searchControl.valueChanges.pipe(takeUntil(this.terminator)).subscribe(controlValue => {
      if (this.skipFirstChange && this.firstChange) {
        this.logger.debug('Skip the first change');
        this.touched = false;
        this.firstChange = false;
        return;
      }
      if (controlValue) {
        if (typeof controlValue === 'string') {
          this.logger.debug('The control value is a search string. Search for entities containing it', controlValue);
          this.dataSource.loadEntities([{name: this.meta.defaultSortField, rawValue: controlValue}],
            this.meta.defaultSortField, this.meta.defaultSortDirection, 0, this.meta.defaultPageSize);
        } else if (controlValue.id) {
          this.logger.debug('The control value is an entity. Set the value directly');
          this.value = controlValue;
        } else {
          this.logger.warn('Control value not recognized: "' + controlValue + '". This should not happen.');
        }
      } else {
        this.logger.debug('No control value provided. Clearing the value');
        this.dataSource.clearEntities();
        this.value = null;
      }
    });


    this.dataSource.totalSubject.pipe(takeUntil(this.terminator)).subscribe(total => {
      if (total === 0 && typeof this.searchControl.value === 'string' && this.searchControl.value.length > this.lengthToShowCreate) {
        this.createButtonVisible = true;
      } else {
        this.createButtonVisible = false;
      }
    });
  }


  createNew() {
    this.logger.info(`Create a new ${this.meta.displayName.toLowerCase()} with value ${this.searchControl.value}`);
    // const queryParams: Params = {[this.meta.defaultSortField]: this.searchControl.value};
    this.service.save({[this.meta.defaultSortField]: this.searchControl.value} as T)
      .pipe(take(1)).subscribe((savedEntity: T) => {
        this.logger.info(`New ${this.meta.displayName.toLowerCase()} created`);
    });
    // this.router.navigate([this.meta.apiBase + '/new'], {queryParams: queryParams});
  }

  clearValue(): void {
    this.value = null;
  }

  // Custom methods
  setValue(value: T) {
    if (this.searchControl.value !== value) {
      this.searchControl.setValue(value);
    }
  }

  isEmpty() {
    return !this.searchControl.value && !this.value;
  }

  setControlDisabledState(isDisabled: boolean): void {
    if (isDisabled) {
      this.searchControl.disable();
    } else {
      this.searchControl.enable();
    }
  }

  setUpControl(): void {
    this.activateControl();
  }

}
