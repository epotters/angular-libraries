import {Component, Injector, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';
import {NGXLogger} from 'ngx-logger';
import {FieldEditorConfig, Identifiable} from '.';
import {BaseEditorRowComponent} from './base-editor-row.component';


export const InlineEditorTooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 1000,
  touchendHideDelay: 1000
};


@Component({
  selector: 'editor-row',
  templateUrl: './editor-row.component.html',
  styleUrls: ['./editor-row.component.css'],
  providers: [
    {provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: InlineEditorTooltipDefaults}
  ],
})
export class EditorRowComponent<T extends Identifiable> extends BaseEditorRowComponent<T> implements OnInit {

  visible = false;

  constructor(
    public formBuilder: FormBuilder,
    injector: Injector,
    public  logger: NGXLogger) {
    super(formBuilder, injector);
    this.logger.debug('Creating EditorRowComponent');
    this.enableValidation = true;
  }

  ngOnInit(): void {
    this.logger.debug(`Initializing EditorRowComponent for type ${this.meta.displayName}`);
  }


  public loadEntity(entity: T) {
    if (entity) {
      this.logger.debug('Entity to load in the row editor:', entity);
      const editorEntity: Partial<T> = this.prepareEntity(entity);
      this.logger.debug('Prepared entity to load in the row editor:', editorEntity);
      this.rowEditorForm.setValue(editorEntity);
      this.rowEditorForm.markAsPristine();
    } else {
      this.logger.debug('Clearing the editor');
      this.rowEditorForm.reset();
    }
  }

  getColumns(): Record<string, FieldEditorConfig> {
    const editorColumns: Record<string, FieldEditorConfig> = {};
    for (const idx in this.columns) {
      const key: string = this.columns[idx];
      editorColumns[key] = this.getColumnEditor(key);
    }
    return editorColumns;
  }

  // Only include fields that are in the form
  private prepareEntity(entity: T): Partial<T> {
    const editorEntity: Partial<T> = {};
    Object.entries(entity).forEach(
      ([key, value]) => {
        if (this.rowEditorForm.contains(key)) {
          // @ts-ignore
          editorEntity[key] = entity[key];
        } else {
          this.logger.debug(`Skipping field "${key}" because there is no control for it.`);
        }
      }
    );
    return editorEntity;
  }

}

