import {Component, Injector} from '@angular/core';
import {AbstractControl, FormBuilder} from '@angular/forms';
import {NGXLogger} from 'ngx-logger';
import {ColumnConfig, FieldEditorConfig, Identifiable} from '.';
import {FieldFilter} from '../domain/filter.model';
import {BaseEditorRowComponent} from './base-editor-row.component';


@Component({
  selector: 'filter-row',
  templateUrl: './editor-row.component.html',
  styleUrls: ['./editor-row.component.css']
})
export class FilterRowComponent<T extends Identifiable> extends BaseEditorRowComponent<T> {

  constructor(
    public formBuilder: FormBuilder,
    injector: Injector,
    public  logger: NGXLogger) {
    super(formBuilder, injector);

    this.logger.debug('Creating FilterRowComponent');
  }

  getOutputValue(): FieldFilter[] {
    return this.getFilters();
  }


  public getFilters(): FieldFilter[] {

    this.logger.debug('Start collecting field filters');
    const entityFilter: any = this.rowEditorForm.getRawValue();
    const fieldFilters: FieldFilter[] = [];

    Object.entries(entityFilter).forEach(
      ([key, value]) => {
        if (!!value && value !== '') {
          let fieldName: string = key;
          const type: any = this.getEditor(key).type;
          const columnConfig: ColumnConfig = this.meta.columnConfigs[key];
          if (type === 'entity-selector') {
            if (!(value as Identifiable)['id']) {
              this.logger.debug(`No related entity to search for selected yet. Skipping ${value}.`);
              return;
            }
            fieldName = fieldName + '.id';
            value = (value as Identifiable)['id'];
            this.logger.debug('Selected entity converted to id only for filter {', fieldName, ': ', value, '}');
          } else if (type === 'text' && !!columnConfig.editor && !!columnConfig.editor.relatedEntity) {
            this.logger.debug('Searching related field ' + fieldName + ' as text');
            fieldName = fieldName + '.' + columnConfig.editor.relatedEntity.displayField;
          }
          fieldFilters.push({
            name: fieldName,
            rawValue: (value as string)
          });
        }
      }
    );
    this.logger.debug('Finished building form output value: ', fieldFilters);
    return fieldFilters;
  }


  public setFilters(fieldFilters: FieldFilter[]): void {
    this.rowEditorForm.reset();
    for (const idx in fieldFilters) {
      const fieldFilter: FieldFilter = fieldFilters[idx];
      const formControl: AbstractControl | null = this.rowEditorForm.get(fieldFilter.name);
      if (!!formControl) {
        formControl.setValue(fieldFilter.rawValue, {emitEvent: false});
        this.logger.debug(`FieldFilter ${fieldFilter.name} set to ${fieldFilter.rawValue}`);
      } else {
        this.logger.debug(`No field control for ${fieldFilter.name}. Skipping.`);
      }
    }
  }


  getColumns(): Record<string, FieldEditorConfig> {
    const editorColumns: Record<string, FieldEditorConfig> = {};
    for (const idx in this.columns) {
      const key: string = this.columns[idx];
      const columnConfig: ColumnConfig = this.meta.columnConfigs[key];
      if (columnConfig && columnConfig.filter) {
        editorColumns[key] = columnConfig.filter;
      } else {
        editorColumns[key] = this.getColumnEditor(key);
      }
    }
    return editorColumns;
  }


  getEditor(key: string): FieldEditorConfig {
    return (this.editorColumns[key]) ? this.editorColumns[key] : this.defaultFieldEditorConfig;
  }

}
