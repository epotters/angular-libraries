import {Component, Input} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {MatDialogRef} from '@angular/material/dialog/dialog-ref';
import {MatSnackBar} from '@angular/material/snack-bar';
import {NGXLogger} from 'ngx-logger';
import {BehaviorSubject, Observable} from 'rxjs';
import {take} from 'rxjs/operators';

import {EntityMeta, Identifiable} from '.';
import {EntityLibConfig} from '../common/entity-lib-config';
import {ConfirmationDialogComponent} from '../dialog/confirmation-dialog.component';
import {EntityService} from '../entity.service';


export interface ActionResult<T> {
  success: boolean;
  changes: boolean;
  msg: string;
  entity?: T
}


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'editor-actions',
  template: ''
})
export class EntityEditorActionsComponent<T extends Identifiable> {

  @Input() meta: EntityMeta<T>;
  @Input() service: EntityService<T>;

  constructor(
    public dialog: MatDialog,
    public snackbar: MatSnackBar,
    public logger: NGXLogger
  ) {
  }


  saveEntity(entityForm: FormGroup, overlay?: any): Observable<ActionResult<T>> {

    const savedEntitySubject: BehaviorSubject<ActionResult<T>> = new BehaviorSubject<ActionResult<T>>(
      {success: false, changes: false, msg: 'BehaviorSubject for entity saving created'}
    );

    if (entityForm.valid) {

      const entity: T = entityForm.getRawValue();

      for (const idx in Reflect.ownKeys(overlay)) {
        const key = Reflect.ownKeys(overlay)[idx] as string;
        // @ts-ignore
        entity[key] = overlay[key];
        this.logger.debug('Apply overlay before saving', key, overlay[key]);
      }

      this.logger.debug('Ready to save', this.meta.displayName, ':', entity);
      this.service.save(entity)
        .pipe(take(1)).subscribe((savedEntity) => {
        let msg: string;
        if (entity.id) {
          msg = `${this.meta.displayName}  named ${this.meta.displayNameRenderer(entity)} is updated successfully`;
        } else {
          msg = `${this.meta.displayName}  named ${this.meta.displayNameRenderer(entity)} is  created successfully with id ${savedEntity.id}`;
        }

        this.logger.info(msg);
        this.snackbar.open(msg, undefined, {
          duration: EntityLibConfig.defaultSnackbarDuration
        });

        this.logger.debug('Before marking the form as pristine, is it dirty?', entityForm.dirty);
        entityForm.markAsPristine();
        entityForm.markAsUntouched();

        this.logger.debug('After marking the form as pristine, is it still dirty?', entityForm.dirty);
        savedEntitySubject.next({success: true, changes: true, msg, entity: savedEntity});
      });
    } else {
      const msg = 'Not a valid ' + this.meta.displayName.toLowerCase() + '. Please correct the errors before saving';
      this.logger.info(msg);
      this.logger.debug('Validation errors', entityForm.errors);
      this.listInvalidFields(entityForm);
      savedEntitySubject.next({success: false, changes: false, msg});
    }
    return savedEntitySubject.asObservable();
  }


  deleteEntity(entity: T): Observable<ActionResult<T>> {

    const deleteEntitySubject: BehaviorSubject<ActionResult<T>> = new BehaviorSubject<ActionResult<T>>(
      {success: false, changes: false, msg: 'BehaviorSubject for entity deleting created'}
    );

    if ((!entity || !entity.id)) {
      const msg: string = 'This entity is either not available or not yet created and therefore cannot be deleted';
      this.logger.info(msg);

      this.snackbar.open(msg, undefined, {
        duration: EntityLibConfig.defaultSnackbarDuration
      });
      deleteEntitySubject.next({success: false, changes: false, msg});
    }

    const dialogRef = this.openConfirmationDialog('Confirm delete ' + this.meta.displayName.toLowerCase(),
      `Are you sure you want to delete "${this.meta.displayNameRenderer(entity)}"?`);

    dialogRef.afterClosed().pipe(take(1)).subscribe(
      data => {
        this.logger.debug('Dialog output:', data);
        if (data.confirmed) {

          this.logger.info('User confirmed delete action, so it will be executed');

          return this.service.delete(('' + entity.id))
            .pipe(take(1)).subscribe(response => {
              const msg = this.meta.displayName + ' named ' + this.meta.displayNameRenderer(entity) + ' was deleted successfully';
              this.logger.info(msg);
              this.snackbar.open(msg, undefined, {
                duration: EntityLibConfig.defaultSnackbarDuration
              });
              deleteEntitySubject.next({success: true, changes: true, msg});
            });

        } else {
          const msg = 'User canceled delete action';
          this.logger.info(msg);
          deleteEntitySubject.next({success: false, changes: false, msg});
        }
      }
    );

    return deleteEntitySubject.asObservable();
  }


  public handleUnsavedChanges(entityForm: FormGroup, overlay?: any): Observable<ActionResult<T>> {
    const unsavedChangesSubject: BehaviorSubject<ActionResult<T>> = new BehaviorSubject<ActionResult<T>>(
      {success: false, changes: false, msg: 'BehaviorSubject for unsaved changes created'}
    );
    if (entityForm.dirty) {
      const msg = 'The editor row has unsaved changes';
      this.logger.debug(msg);
      if (EntityLibConfig.autoSave) {
        this.saveSilently(unsavedChangesSubject, entityForm, overlay);
      } else {
        const dialogRef = this.openConfirmationDialog('Unsaved changes', 'Do you want to save the changes?');
        dialogRef.afterClosed().pipe(take(1)).subscribe(
          data => {
            if (data.confirmed) {
              this.logger.info('User confirmed he wants to save changes to ' + this.meta.displayName.toLowerCase());
              this.saveSilently(unsavedChangesSubject, entityForm, overlay);
            } else {
              const msg = 'User chose to discard changes in the editor';
              this.logger.info(msg);
              unsavedChangesSubject.next({success: true, changes: false, msg});
            }
          }
        );
      }

    } else {
      const msg = 'The editor row doesn\'t have unsaved changes';
      this.logger.debug(msg);
      unsavedChangesSubject.next({success: true, changes: false, msg});
    }
    return unsavedChangesSubject.asObservable();
  }

  updateEntities() {
    const dialogRef: MatDialogRef<any> = this.openConfirmationDialog('Confirm batch update',
      'Are you sure you want to update all selected ' + this.meta.displayNamePlural.toLowerCase() + '?');
    dialogRef.afterClosed().pipe(take(1)).subscribe(
      data => {
        if (data.confirmed) {
          this.logger.info('User confirmed batch update action, so it will be executed');
          // this.actionResult.emit({action: 'save', result: 'ok'});
        } else {
          this.logger.info('User canceled batch update action');
        }
      }
    );
  }

  deleteEntities() {
    const dialogRef: MatDialogRef<any> = this.openConfirmationDialog('Confirm batch deletion',
      'Are you sure you want to delete all selected ' + this.meta.displayNamePlural.toLowerCase() + '?');
    dialogRef.afterClosed().pipe(take(1)).subscribe(
      data => {
        if (data.confirmed) {
          this.logger.info('User confirmed batch delete action, so it will be executed');
          // this.actionResult.emit({action: 'save', result: 'ok'});
        } else {
          this.logger.info('User canceled batch delete action');
        }
      }
    );
  }

  openConfirmationDialog(title: string, message: string): MatDialogRef<ConfirmationDialogComponent> {
    const dialogConfig: MatDialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = false;
    dialogConfig.data = {title, message};
    return this.dialog.open(ConfirmationDialogComponent, dialogConfig);
  }

  private saveSilently(unsavedChangesSubject: BehaviorSubject<ActionResult<T>>, entityForm: FormGroup, overlay?: any) {
    this.saveEntity(entityForm, overlay)
      .pipe(take(1)).subscribe((result: ActionResult<T>) => {
      const msg = 'Entity saved, no more unsaved changes';
      this.logger.debug(msg);
      unsavedChangesSubject.next(result);
    });
  }


  private listInvalidFields(entityForm: FormGroup): void {
    const controls = entityForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        this.logger.debug(`Field ${name} is invalid`, controls[name].value, controls[name].errors);
      }
    }
  }


}
