import {HttpParams} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {FilterBuilder} from './common/filter/filter-builder';
import {ApiService} from './domain/api-service.model';
import {EntityMeta} from './domain/entity-meta.model';
import {EntityResult} from './domain/entity-result.model';

import {FieldFilter} from './domain/filter.model';
import {Identifiable} from './domain/identifiable.model';


export class EntityService<T extends Identifiable> {

  filterBuilder: FilterBuilder<T>;

  constructor(
    public meta: EntityMeta<T>,
    public apiService: ApiService,
    public  logger: NGXLogger
  ) {
    logger.debug(`Start constructing an EntityService for type ${meta.displayName}`);
    this.filterBuilder = new FilterBuilder<T>(meta, logger);
  }

  public list(filters?: FieldFilter[], sortField = 'id', sortDirection = 'asc', pageNumber = 0, pageSize = 100): Observable<any> {

    const params: HttpParams = new HttpParams()
      .set('filters', this.filterBuilder.buildFilterParams(filters))
      .set('sort', sortField + ',' + sortDirection)
      .set('page', pageNumber.toString())
      .set('size', pageSize.toString());

    return this.apiService.get(this.meta.apiBase, params)
      .pipe(
        map((response: Response) => {
          const result: EntityResult<T> = {
            entities: response['content'],
            total: response['totalElements']
          };
          return result;
        })
      );
  }

  public get(id: string): Observable<any> {
    this.logger.debug(`About to get ${this.meta.displayName.toLowerCase()} with id  ${id}`);
    return this.apiService.get(this.meta.apiBase + id)
      .pipe(map((response: Response) => {
        return response;
      }));
  }

  public save(entity: T): Observable<T> {
    if (entity.id) { // Update existing entity
      this.logger.debug(`About to update ${this.meta.displayName.toLowerCase()} with id ${entity.id}`);
      return this.apiService.post(this.meta.apiBase + entity.id, entity);
    } else { // Create new entity
      this.logger.debug(`About to create a new ${this.meta.displayName.toLowerCase()}`);
      return this.apiService.put(this.meta.apiBase, entity);
    }
  }

  public delete(id: string): Observable<T> {
    this.logger.debug(`About to delete ${this.meta.displayName.toLowerCase()} with id ${id}`);
    return this.apiService.delete(this.meta.apiBase + id);
  }

}
