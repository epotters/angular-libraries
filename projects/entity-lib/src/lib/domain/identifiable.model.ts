/* Inherited by all entities */
export interface Identifiable {
  [key: string]: any;
  id: number;
}
