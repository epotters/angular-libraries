import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {EntityCommonModule} from './common/entity-common.module';
import {EntityComponentStyleOverrideDirective} from './common/entity-component-style-override.directive';
import {EntityLinkDirective} from './common/entity-link.directive';
import {EntityMaterialModule} from './common/entity-material.module';
import {DialogModule} from './dialog/dialog.module';
import {EntityRelativeNavigationComponent} from './entity-relative-navigation.component';
import {EntitySelectorModule} from './entity-selector/entity-selector.module';
import {TableRowEditorModule} from './table-row-editor/table-row-editor.module';


@NgModule({
  imports: [
    CommonModule,
    EntityCommonModule,
    EntityMaterialModule,
    TableRowEditorModule,
    EntitySelectorModule,
    DialogModule
  ],
  exports: [
    EntityCommonModule,
    EntityMaterialModule,
    TableRowEditorModule,
    EntitySelectorModule,
    DialogModule,
    EntityRelativeNavigationComponent
  ],
  declarations: [
    EntityRelativeNavigationComponent,
    EntityComponentStyleOverrideDirective,
    EntityLinkDirective
  ]
})

export class EntityModule {
}
