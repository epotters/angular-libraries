import {Directive, Input, OnChanges, SimpleChanges} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {ColumnConfig, EntityMeta, RelationEntity} from './domain/entity-meta.model';
import {FieldFilter} from './domain/filter.model';
import {Identifiable} from './domain/identifiable.model';
import {EntityService} from './entity.service';

@Directive()
// tslint:disable-next-line:directive-class-suffix
export abstract class EntityDetailComponent<T extends Identifiable> implements OnChanges {

  @Input() entity: T;

  protected constructor(
    public meta: EntityMeta<T>,
    public service: EntityService<T>,
    public logger: NGXLogger
  ) {
    logger.debug('Constructing the EntityDetailComponent for type ' + this.meta.displayName);
  }


  ngOnChanges(changes: SimpleChanges) {
    this.logger.debug('Detail ngOnChanges', changes);
  }


  // WIP
  listRelationsInline(fieldName: string): string {
    const columnConfig: ColumnConfig = this.meta.columnConfigs[fieldName];
    const relation: RelationEntity | undefined = (columnConfig && columnConfig.editor) ? columnConfig.editor.relationEntity : undefined;
    const html = '';
    if (!relation) {
      return '';
    }

    const filter: FieldFilter = {
      name: relation.owner,
      rawValue: `${this.entity.id}`
    };


    return html;
  }


}
