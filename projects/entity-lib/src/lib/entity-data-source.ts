import {DataSource} from '@angular/cdk/collections';
import {NGXLogger} from 'ngx-logger';

import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, finalize} from 'rxjs/operators';
import {EntityMeta} from './domain/entity-meta.model';

import {FieldFilter} from './domain/filter.model';
import {Identifiable} from './domain/identifiable.model';
import {EntityService} from './entity.service';


export class EntityDataSource<T extends Identifiable> implements DataSource<T> {

  public entitiesSubject: BehaviorSubject<T[]> = new BehaviorSubject<T[]>([]);
  public totalSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(
    public meta: EntityMeta<T>,
    public service: EntityService<T>,
    public logger: NGXLogger
  ) {
    this.logger.debug(`Creating EntityDataSource for type ${this.meta.displayName}`);
  }

  public connect(): Observable<T[]> {
    this.logger.debug(`Connecting the ${this.meta.displayNamePlural.toLowerCase()} datasource...`);
    return this.entitiesSubject.asObservable();
  }

  public disconnect(): void {
    this.logger.debug(`Disconnecting the ${this.meta.displayNamePlural.toLowerCase()} datasource...`);
    this.entitiesSubject.complete();
    this.totalSubject.complete();
    this.loadingSubject.complete();
  }

  public loadEntities(
    filter: FieldFilter[],
    sortField: string,
    sortDirection: string,
    pageNumber: number,
    pageSize: number): void {

    this.logger.debug(`Datasource loading ${this.meta.displayNamePlural.toLowerCase()} ...`);
    this.loadingSubject.next(true);

    this.service.list(filter, sortField, sortDirection, pageNumber, pageSize)
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(entityResult => {

        this.logger.debug(`Datasource ${this.meta.displayNamePlural.toLowerCase()} received result`, entityResult);

        this.entitiesSubject.next(entityResult.entities);
        this.totalSubject.next(entityResult.total);
        this.loadingSubject.next(false);

        this.logger.debug(`Datasource ${this.meta.displayNamePlural.toLowerCase()} processed result`);
      });
  }

  public clearEntities() {
    this.entitiesSubject.next([]);
    this.totalSubject.next(0);
    this.loadingSubject.next(false);
  }
}
