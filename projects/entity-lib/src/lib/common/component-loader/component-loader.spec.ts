import {DatePipe} from '@angular/common';
import {ComponentFactoryResolver} from '@angular/core';
import {TestBed} from '@angular/core/testing';
import {LoggerConfig, NGXLogger, NGXLoggerHttpService, NgxLoggerLevel, NGXMapperService} from 'ngx-logger';
import {NGXLoggerHttpServiceMock, NGXMapperServiceMock} from 'ngx-logger/testing';
import {ComponentLoader} from './component-loader';


beforeEach(() => {
  TestBed.configureTestingModule({
    declarations: [],
    providers: [
      ComponentFactoryResolver,
      NGXLogger,
      {provide: NGXLoggerHttpService, useClass: NGXLoggerHttpServiceMock},
      {provide: NGXMapperService, useClass: NGXMapperServiceMock},
      {provide: LoggerConfig, useValue: {level: NgxLoggerLevel.ERROR}},
      DatePipe]
  }).compileComponents();
});


describe('ComponentLoader tests', () => {
  let componentFactoryResolver: ComponentFactoryResolver;
  let logger: NGXLogger;
  beforeEach(() => {
    componentFactoryResolver = TestBed.get(ComponentFactoryResolver);
    logger = TestBed.get(NGXLogger);
  });


  it('should load an entity component', () => {
    const loader = new ComponentLoader(componentFactoryResolver, logger);

  });

});
