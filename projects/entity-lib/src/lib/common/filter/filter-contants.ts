

export class FilterConstants {
  static readonly exactMatchOperator = ':';
  static readonly likeOperator = '~';
  static readonly rangeDenominator: string = '...';
}
