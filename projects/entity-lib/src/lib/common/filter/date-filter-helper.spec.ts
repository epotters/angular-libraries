import {DatePipe} from '@angular/common';
import {TestBed} from '@angular/core/testing';
import {LoggerConfig, NGXLogger, NGXLoggerHttpService, NgxLoggerLevel, NGXMapperService} from 'ngx-logger';
import {NGXLoggerHttpServiceMock, NGXMapperServiceMock} from 'ngx-logger/testing';
import {DateFilterHelper} from './date-filter-helper';


describe('DateFilter tests', () => {

  let logger: NGXLogger;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        NGXLogger,
        {provide: NGXLoggerHttpService, useClass: NGXLoggerHttpServiceMock},
        {provide: NGXMapperService, useClass: NGXMapperServiceMock},
        {provide: LoggerConfig, useValue: {level: NgxLoggerLevel.ERROR}},
        DatePipe]
    }).compileComponents();

    logger = TestBed.get(NGXLogger);

  });

  it('should validate terms for date searches', () => {
    const helper = new DateFilterHelper(logger);

    expect(helper.processDateTerm('1967-03-23').valid).toBe(true);
    expect(helper.processDateTerm('1967-03').valid).toBe(true);
    expect(helper.processDateTerm('1967-03-').valid).toBe(true);
    expect(helper.processDateTerm('1967-03-2').valid).toBe(true);
    expect(helper.processDateTerm('1967-15-').valid).toBe(false);
    expect(helper.processDateTerm('1967').valid).toBe(true);
    expect(helper.processDateTerm('1967-').valid).toBe(true);
    expect(helper.processDateTerm('1967-0').valid).toBe(true);
    expect(helper.processDateTerm('03-23').valid).toBe(true);
    expect(helper.processDateTerm('15-23').valid).toBe(false);
    expect(helper.processDateTerm('23-03-1967').valid).toBe(true);

    expect(helper.parse('1967...1970').valid).toBe(true);
    expect(helper.parse('1967-03...1970-05').valid).toBe(true);
    expect(helper.parse('1967-03-23...1971-08-20').valid).toBe(true);
  });

});
