import {Injectable} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {UserManagerSettings} from 'oidc-client';


export interface AuthConfig {
  userManagerSettings: UserManagerSettings;
}


/*
This service should be replaced by an application global ConfigService that implements the AuthConfig interface
 */

@Injectable({
  providedIn: 'any'
})
export class AuthConfigService implements AuthConfig {

  userManagerSettings!: UserManagerSettings;

  constructor(public logger: NGXLogger) {
    logger.info('Constructing AuthConfigService. This message should not appear.');
  }
}

