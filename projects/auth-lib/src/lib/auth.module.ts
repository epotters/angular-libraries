import {NgModule} from '@angular/core';

import {AuthGuardService} from './auth-guard.service';
import {AuthLoginCallbackComponent} from './auth-login-callback.component';

import {AuthLoginComponent} from './auth-login.component';
import {AuthLogoutCallbackComponent} from './auth-logout-callback.component';
import {AuthLogoutComponent} from './auth-logout.component';

import {AuthRoutingModule} from './auth-routing.module';
import {AuthService} from './auth.service';


@NgModule({
  imports: [AuthRoutingModule],
  declarations: [
    AuthLoginComponent,
    AuthLogoutComponent,
    AuthLoginCallbackComponent,
    AuthLogoutCallbackComponent
  ],
  entryComponents: [
    AuthLoginComponent,
    AuthLogoutComponent,
    AuthLoginCallbackComponent,
    AuthLogoutCallbackComponent
  ],
  providers: [
    AuthGuardService,
    AuthService
  ]

})
export class AuthModule {
}
