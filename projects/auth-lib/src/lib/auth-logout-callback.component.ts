import {Component, OnInit} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-auth-logout-callback',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthLogoutCallbackComponent implements OnInit {

  message: string = 'Auth Logout Callback Page';

  constructor(
    private authService: AuthService,
    public logger: NGXLogger) {
  }

  ngOnInit() {
    this.logger.debug(this.message);
    this.authService.completeLogout()
      .then(() => this.logger.info('Logout completed successfully'));
  }

}
