import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {NGXLogger} from 'ngx-logger';
import {User, UserManager} from 'oidc-client';

import {AuthConfigService} from './auth-config.service';


export {User};


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly userManager: UserManager;
  private user: User | null = null;
  private readonly returnUrlKey: string = 'auth:redirect';
  private readonly logoutFrameId = 'logout-frame';


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private config: AuthConfigService,
    public logger: NGXLogger) {
    this.logger.info('Constructing the AuthService');
    this.userManager = new UserManager(config.userManagerSettings);
    this.registerEventlisteners();
  }

  // Source: https://www.scottbrady91.com/Angular/SPA-Authentiction-using-OpenID-Connect-Angular-CLI-and-oidc-client
  public isLoggedIn(): boolean {
    return !!this.user && !this.user.expired;
  }

  public isExpired(): boolean {
    return !!this.user && this.user.expired;
  }

  public getClaims(): any {
    if (!!this.user && !!this.user.profile) {
      return this.user.profile;
    } else {
      return null;
    }
  }

  public getAuthorizationHeaderValue(): string | null {
    if (!!this.user) {
      // Bearer is case sensitive, token_type is lowercase, doesn't work
      return AuthService.capitalizeFirst(this.user.token_type) + ' ' + this.user.access_token;
    } else {
      return null;
    }
  }

  public startAuthentication(returnUrl: string): Promise<void> {
    this.setReturnUrl(returnUrl);
    return this.userManager.signinRedirect();
  }

  public completeAuthentication(): Promise<void> {
    return this.userManager.signinRedirectCallback().then(user => {
      this.user = user;
      this.logger.debug('Authentication completed');
      this.returnToUrl();
    });
  }

  public startSilentAuthentication(returnUrl?: string): Promise<User> {
    this.logger.debug(`Silent authentication - About to set return url to "${returnUrl || this.router.url}"`);
    this.setReturnUrl(returnUrl || this.router.url);
    return this.userManager.signinSilent();
  }

  public startLogout(): Promise<void> {
    this.route.url.subscribe(url => {
      this.setReturnUrl(url.join('/'));
      this.logger.info(`Return url was set to "${this.getReturnUrl()}"`);
    });
    return this.userManager.signoutRedirect();
  }

  public completeLogout(): Promise<void> {
    return this.userManager.signoutRedirectCallback().then(() => {
      this.user = null;
      this.returnToUrl();
    });
  }

  public startSilentLogout() {
    this.userManager.createSignoutRequest().then(signoutRequest => {
      this.logger.debug(`Signout URL is "${signoutRequest.url}"`);
      const iframe: HTMLIFrameElement | null = this.getSessionStatusFrame();
      if (iframe) {
        iframe.setAttribute('src', signoutRequest.url);
      } else {
        this.logger.warn('Session status iframe not found');
      }
    });
  }

  private getSessionStatusFrame(): HTMLIFrameElement | null {
    let sessionStatusFrame: HTMLIFrameElement | null = null;
    const iframes: NodeListOf<HTMLIFrameElement> = document.querySelectorAll('iframe');
    iframes.forEach((iframe, idx) => {
      if (iframe.getAttribute('src').indexOf('login-status-iframe.html') > 0) {
        sessionStatusFrame = iframe;
      }
    });
    return sessionStatusFrame;
  }

  private getLogoutFrame(): HTMLIFrameElement {
    let iframe: HTMLElement | null = document.getElementById(this.logoutFrameId);
    if (!iframe) {
      iframe = document.createElement('iframe');
      iframe.setAttribute('id', this.logoutFrameId);
      iframe.setAttribute('class', 'hidden-frame');
      document.body.append(iframe);
      iframe.addEventListener('load', (event) => {
        this.logger.debug('Logout frame load event from parent', event);
        this.userManager.removeUser().then(() => {
          this.logger.debug('UserManager user removed');
          this.user = null;
          this.returnToUrl();
        });
      });
    }
    return (iframe as HTMLIFrameElement);
  }

  public setReturnUrl(returnUrl: string): void {
    this.logger.debug(`Setting return url to "${returnUrl}"`);
    sessionStorage.setItem(this.returnUrlKey, returnUrl);
  }

  public getReturnUrl(): string | null {
    return sessionStorage.getItem(this.returnUrlKey);
  }

  public returnToUrl(): void {
    const returnUrl: string | null = this.getReturnUrl();
    if (!!returnUrl) {
      this.logger.debug(`Returning to URL "${returnUrl}"`);
      sessionStorage.removeItem(this.returnUrlKey);
      this.router.navigate([returnUrl]);
    } else {
      this.router.navigate(['']);
    }
  }


  private registerEventlisteners() {

    this.userManager.events.addUserLoaded(() => {
      this.userManager.getUser().then(user => {
        this.user = user;
        this.logger.info(`User "${((!!user) ? user.profile.name : 'unknown')}" loaded. ` +
          `User session has been established (or re-established)`);
      });
    });

    this.userManager.events.addUserSessionChanged(() => {
      this.logger.info('User session changed');
    });

    this.userManager.events.addAccessTokenExpiring(() => {
      this.logger.info('Access token is about to expire');
    });

    this.userManager.events.addAccessTokenExpired(() => {
      this.logger.info('Access token has expired');
    });

    this.userManager.events.addSilentRenewError(() => {
      this.logger.info('The automatic silent renew failed. Redirect to login page.');
      this.startAuthentication(this.router.url);
    });

    this.userManager.events.addUserSignedOut(() => {
      this.logger.info('User signed out. The user\'s sign-in status at the OIDC Provider has changed');
    });

    this.userManager.events.addUserUnloaded(() => {
      this.logger.info('User unloaded. The user\'s session has been terminated');
    });
  }

  private static capitalizeFirst(text: string): string {
    return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
  }

}
