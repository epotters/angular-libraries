import {Component, OnInit} from '@angular/core';
import {NGXLogger} from 'ngx-logger';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-auth-logout',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthLogoutComponent implements OnInit {

  message: string = 'Auth Logout Page';

  constructor(
    private authService: AuthService,
    public logger: NGXLogger) {
  }

  ngOnInit() {
    this.logger.debug(this.message);
    this.authService.startLogout();
  }

}
